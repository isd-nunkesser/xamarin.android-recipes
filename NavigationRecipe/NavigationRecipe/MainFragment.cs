﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace NavigationRecipe
{
    public class MainFragment : Android.Support.V4.App.Fragment
    {
        private const String ARG_PARAM1 = "param1";
        private String Param1;
        private IOnFragmentInteractionListener Listener;

        public static MainFragment NewInstance(String param1)
        {
            var f = new MainFragment();
            var args = new Bundle();
            args.PutString(ARG_PARAM1, param1);
            f.Arguments = args;
            return f;
        }

        public override void OnAttach(Context context)
        {
            base.OnAttach(context);
            if (context is IOnFragmentInteractionListener)
            {
                Listener = (NavigationRecipe.MainFragment.IOnFragmentInteractionListener)context;
            } else
            {
                throw new AndroidRuntimeException($"{context.ToString()} must implement IOnFragmentInteractionListener");
            }
        }

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            if (Arguments != null)
            {
                Param1 = Arguments.GetString(ARG_PARAM1);
            }
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var view = inflater.Inflate(Resource.Layout.fragment_main, container, false);
            var textView = view.FindViewById<TextView>(Resource.Id.mainTextView);
            textView.Text = Param1;

            var button = view.FindViewById<Button>(Resource.Id.nextFragmentButton);
            button.Click += ButtonOnClick;

            return view;
        }

        private void ButtonOnClick(object sender, EventArgs eventArgs)
        {
            Listener.NavigateToNextFragment();
        }

        public interface IOnFragmentInteractionListener
        {
            void NavigateToNextFragment();
        }
    }
}
