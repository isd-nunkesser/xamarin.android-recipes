﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using BasicCleanArch;

namespace RESTRecipe
{
    public class MainFragment : Android.Support.V4.App.Fragment, IDisplayer<string>
    {
        private readonly IUseCase<object, string> _getInteractor =
            new HttpBinGetInteractor(new HttpBinGateway(),
                new HttpBinResponsePresenter());

        private readonly IUseCase<HttpBinPostRequest, string> _postInteractor =
            new HttpBinPostInteractor(new HttpBinGateway(),
                new HttpBinResponsePresenter());

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            return inflater.Inflate(Resource.Layout.fragment_main, container, false);

            
        }

        public override void OnViewCreated(View view, Bundle savedInstanceState)
        {
            base.OnViewCreated(view, savedInstanceState);
            _getInteractor.Execute(null, this);
        }

        public void Display(Result<string> response)
        {
            response.Match(
                success => Log.Info("MainFragment", success),
                failure => Log.Error("MainFragment", failure.ToString())
                    ); 
        }
    }
}
