﻿using System;
namespace RESTRecipe
{
    public class HttpBinResponseModel
    {
        public string origin { get; set; }
        public string url { get; set; }
        public HttpBinPostRequest json { get; set; }
    }
}
