﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V4.App;
using Android.Support.V4.Content;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace RuntimePermissionsRecipe
{
    public class MainFragment : Android.Support.V4.App.Fragment
    {
        private readonly string TAG = "MainFragment";
        private View view;

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            return inflater.Inflate(Resource.Layout.fragment_main, container, false);
        }

        public override void OnViewCreated(View view, Bundle savedInstanceState)
        {            
            base.OnViewCreated(view, savedInstanceState);
            this.view = view;
            view.FindViewById<Button>(Resource.Id.takePictureButton).Click += TakePicture_Click;
        }

        private void TakePicture_Click(object sender, EventArgs e)
        {
            if (ContextCompat.CheckSelfPermission(Activity, Manifest.Permission.Camera) == (int)Permission.Granted)
            {
                // We have permission, go ahead and use the camera.
            }
            else
            {
                // Camera permission is not granted. If necessary display rationale & request.
                RequestCameraPermission();
            }
        }

        void RequestCameraPermission()
        {
            Log.Info(TAG, "CAMERA permission has NOT been granted. Requesting permission.");

            if (ActivityCompat.ShouldShowRequestPermissionRationale(Activity, Manifest.Permission.Camera))
            {
                // Provide an additional rationale to the user if the permission was not granted
                // and the user would benefit from additional context for the use of the permission.
                // For example if the user has previously denied the permission.
                Log.Info(TAG, "Displaying camera permission rationale to provide additional context.");

                Snackbar.Make(view, Resource.String.permission_camera_rationale,
                    Snackbar.LengthIndefinite).SetAction(Resource.String.ok, new Action<View>(delegate (View obj) {
                        ActivityCompat.RequestPermissions(Activity, new String[] { Manifest.Permission.Camera }, MainActivity.REQUEST_CAMERA);
                    })).Show();
            }
            else
            {
                // Camera permission has not been granted yet. Request it directly.
                ActivityCompat.RequestPermissions(Activity, new String[] { Manifest.Permission.Camera }, MainActivity.REQUEST_CAMERA);
            }
        }
    }
}
