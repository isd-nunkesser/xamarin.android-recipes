﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Runtime;
using Android.Widget;
using Android.Util;
using Android.Support.Design.Widget;
using Android.Views;
using Android.Content.PM;

namespace RuntimePermissionsRecipe
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        private readonly string TAG = "MainActivity";
        public static readonly int REQUEST_CAMERA = 0;
        View layout;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.activity_main);
            layout = FindViewById(Resource.Id.main_layout);
            if (savedInstanceState==null)
            {
                SupportFragmentManager.BeginTransaction().Replace(Resource.Id.fragment, new MainFragment()).Commit();
            }
        }
        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            if (requestCode == REQUEST_CAMERA)
            {
                // Received permission result for camera permission.
                Log.Info(TAG, "Received response for Camera permission request.");

                // Check if the only required permission has been granted
                if (grantResults.Length == 1 && grantResults[0] == Permission.Granted)
                {
                    // Camera permission has been granted, preview can be displayed
                    Log.Info(TAG, "CAMERA permission has now been granted. Showing preview.");
                    Snackbar.Make(layout, Resource.String.permission_available_camera, Snackbar.LengthShort).Show();
                }
                else
                {
                    Log.Info(TAG, "CAMERA permission was NOT granted.");
                    Snackbar.Make(layout, Resource.String.permissions_not_granted, Snackbar.LengthShort).Show();
                }
            }
        }
    }
}