﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using BasicCleanArch;

namespace MinimalCleanArch
{
    public class MainFragment : Android.Support.V4.App.Fragment, IDisplayer<string>
    {
        private TextView resultTextView;

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            return inflater.Inflate(Resource.Layout.fragment_main, container, false);
        }

        public override void OnViewCreated(View view, Bundle savedInstanceState)
        {
            base.OnViewCreated(view, savedInstanceState);
            resultTextView = view.FindViewById<TextView>(Resource.Id.textView);
            view.FindViewById<Button>(Resource.Id.button).Click += (s, e) =>
            {
                var presenter = new MinimalPresenter();
                var interactor = new MinimalInteractor(presenter);
                interactor.Execute(null, this);
            };
        }

        public void Display(Result<string> result)
        {            
            result.Match(
            (success) => resultTextView.Text = success,
            (failure) => Log.Error("MainFragment",failure.ToString())
            );
        }
    }
}
