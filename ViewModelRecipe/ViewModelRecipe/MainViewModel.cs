﻿using System;
using Android.Arch.Lifecycle;

namespace ViewModelRecipe
{
    public class MainViewModel : ViewModel
    {
        private MutableLiveData data = new MutableLiveData();
        public LiveData Data
        {
            get => data;
        }

        public MainViewModel()
        {
            this.data.SetValue("Hello world");
        }

    }
}
