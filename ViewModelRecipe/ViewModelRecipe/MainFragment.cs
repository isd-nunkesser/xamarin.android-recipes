﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Arch.Lifecycle;
using Java.Lang;

namespace ViewModelRecipe
{
    public class MainFragment : Android.Support.V4.App.Fragment
    {        

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            return inflater.Inflate(Resource.Layout.fragment_main, container, false);
        }

        public override void OnActivityCreated(Bundle savedInstanceState)
        {
            base.OnActivityCreated(savedInstanceState);
            var textView = Activity.FindViewById<TextView>(Resource.Id.messageTextView);
            var viewModel = ViewModelProviders.Of(this).Get(Java.Lang.Class.FromType(typeof(MainViewModel))) as MainViewModel;
            viewModel.Data.Observe(this, new ChangeObserver(textView));
        }

        private class ChangeObserver : Java.Lang.Object, IObserver
        {
            private TextView textView;

            public ChangeObserver(TextView textView)
            {
                this.textView = textView;
            }

            public void OnChanged(Java.Lang.Object p0)
            {
                textView.Text = p0.ToString();
            }
        }
    }
}
