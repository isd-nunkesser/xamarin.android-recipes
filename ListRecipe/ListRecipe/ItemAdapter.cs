﻿using System;
using Android.Support.V7.RecyclerView.Extensions;
using Android.Support.V7.Util;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using Java.Lang;

namespace ListRecipe
{
    public class ItemAdapter : ListAdapter
    {
        public ItemAdapter() : base(new ItemCallback())
        {

        }

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            var item = GetItem(position);
            ((ItemViewHolder)holder).ContentView.Text = item.ToString();
        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            var view = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.fragment_item, parent, false);
            return new ItemViewHolder(view);
        }

        class ItemViewHolder : RecyclerView.ViewHolder
        {
            public TextView ContentView { get; set; }
            public ItemViewHolder(View itemView) : base(itemView)
            {
                ContentView = itemView.FindViewById<TextView>(Resource.Id.content);
            }
        }

        class ItemCallback : DiffUtil.ItemCallback
        {
            public override bool AreContentsTheSame(Java.Lang.Object p0, Java.Lang.Object p1)
            {
                return p0.Equals(p1);
            }

            public override bool AreItemsTheSame(Java.Lang.Object p0, Java.Lang.Object p1)
            {
                return p0 == p1;
            }
        }
    }
}
