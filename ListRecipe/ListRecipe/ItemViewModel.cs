﻿using System;
using System.Collections.Generic;
using Android.Arch.Lifecycle;
using Android.Runtime;

namespace ListRecipe
{
    public class ItemViewModel : ViewModel
    {
        private MutableLiveData data = new MutableLiveData();
        public LiveData Data
        {
            get => data;
        }

        public ItemViewModel()
        {            
            data.SetValue(new JavaList(new string[] { "Teddy bear", "Banana", "Sponge", "Laptop" }));
        }
    }
}
