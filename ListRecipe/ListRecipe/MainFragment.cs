﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Arch.Lifecycle;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Util;
using Android.Views;
using Android.Widget;
using Java.Lang;
using static Android.Support.V7.Widget.SearchView;

namespace ListRecipe
{
    public class MainFragment : Android.Support.V4.App.Fragment
    {
        private ItemAdapter adapter = new ItemAdapter();

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            return inflater.Inflate(Resource.Layout.fragment_main, container, false);
        }

        public override void OnViewCreated(View view, Bundle savedInstanceState)
        {
            base.OnViewCreated(view, savedInstanceState);

            var viewModel = ViewModelProviders.Of(this).Get(Java.Lang.Class.FromType(typeof(ItemViewModel))) as ItemViewModel;
            viewModel.Data.Observe(this, new ActionObserver(
                value => adapter.SubmitList((System.Collections.IList)value)
            ));

            var recyclerView = view.FindViewById<RecyclerView>(Resource.Id.recyclerView);
            recyclerView.SetLayoutManager(new LinearLayoutManager(Activity));
            recyclerView.SetAdapter(adapter);

            var searchView = view.FindViewById<Android.Support.V7.Widget.SearchView>(Resource.Id.searchView);
            searchView.QueryTextChange += (s, e) =>
            {
                var items = (System.Collections.IList)viewModel.Data.Value;
                if (string.IsNullOrWhiteSpace(e.NewText))
                {
                    adapter.SubmitList(items);
                }
                else
                {
                    var filteredItems = new JavaList();
                    foreach (Java.Lang.String item in items)
                    {                        
                        if (item.ToString().IndexOf(e.NewText, StringComparison.OrdinalIgnoreCase) >= 0) {
                            filteredItems.Add(item);
                        }
                    }
                    adapter.SubmitList(filteredItems);
                }

            };
        }

            

        private class ActionObserver : Java.Lang.Object, IObserver
        {
            private Action<Java.Lang.Object> action;

            public ActionObserver(Action<Java.Lang.Object> action)
            {
                this.action = action;
            }

            public void OnChanged(Java.Lang.Object p0)
            {
                action(p0);                
            }
        }
    }
}
